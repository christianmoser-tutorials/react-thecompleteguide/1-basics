import './Person.css'
import React, { Component, Fragment } from 'react'
import PropTypes from 'prop-types'
import AuthContext from '../../../context/auth-context'
export class Person extends Component {
  static propTypes = {
    click: PropTypes.func,
    name: PropTypes.string,
    age: PropTypes.number 
  };

  constructor(props) {
    super(props)
    this.inputElementRef = React.createRef();
  }

  static contextType = AuthContext;

  componentDidMount() {
    this.inputElementRef.current.focus()
    console.log(this.context.authenticated);
    
  }

  render() {
    console.log('[Person.js] rendering...');
    return (
      <Fragment>

        <div className="Person" onClick={this.props.click}>
          {this.context.authenticated ? (
            <p>Authenticated</p>
          ) : (
            <p>Please log in!</p>
          )}
          <AuthContext.Consumer>
            {(context) => context.authenticated ? <p>Authenticated</p> : <p>Please Login!</p>}
          </AuthContext.Consumer>
          <p>My name is {this.props.name} and iam {this.props.age} old!</p>
          <p>{this.props.children}</p>
          <input 
            //ref={(inputEl) => {this.inputElement = inputEl}}
            ref={this.inputElementRef}
            type="text"
            onChange={this.props.changed}
            value={this.props.name}
            onClick={(e) => {e.stopPropagation()}}
            onFocus={() => {console.log('focus')}}
          />
        </div> 
      </Fragment>
    )
  }
}
export default Person

