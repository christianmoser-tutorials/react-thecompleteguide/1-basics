import React, { PureComponent } from 'react'
import Person from './Person/Person'

export class Persons extends PureComponent {

/*   static getDerivedStateFromProps(props, state) {
    console.log('[Persons.js] getDerivedStateFromProps');
    return state;
  } */

/*   shouldComponentUpdate(nextProps, nextState) {
    console.log('[Persons.js] shouldComponentUpdate');
    if (nextProps.persons !== this.props.persons) {
      return true;
    } else {
      return false;
    }
  } */

  getSnapshotBeforeUpdate(prevProps, prevState) {
    console.log('[Persons.js] getSnapshotBeforeUpdate');
    return {message: 'snapshot!'};
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    console.log('[Persons.js] componentDidUpdate');
    console.log(prevProps, prevState, snapshot);
  }

  componentWillUnmount() {
    console.log('[Persons.js] componentWillUnmount');
  }

  render() {
    console.log('[Persons.js] rendering...');
    return this.props.persons.map((value, index) => {
      return ( 
        <Person 
          key={index}
          name={value.name}
          age={value.age} 
          click={() => this.props.clicked(index)}
          changed={(event) => this.props.changed(event, index)}
        ></Person>
      )
    })
  }
}

export default Persons
