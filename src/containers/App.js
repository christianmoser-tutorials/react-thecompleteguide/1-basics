import React, { Component } from 'react'
import classes from './App.module.css'
import Cockpit from '../components/Cockpit/Cockpit'
import Persons from '../components/Persons/Persons'
import WithClass from '../hoc/WithClass'
import AuthContext from '../context/auth-context'
export class App extends Component {

  constructor(props) {
    super(props)
    console.log('[App.js] constructor');
    
  }

  state = {
    persons: [
      {name: 'John', age: 20},
      {name: 'Jenny', age: 25},
      {name: 'Peter', age: 18}
    ],
    showPersons: false,
    changeCounter: 0,
    authenticated: false
  }

  static getDerivedStateFromProps(props, state) {
    console.log('[App.js] getDerivedStateFromProps', props);
    return state;
  }

  componentDidMount() {
    console.log('[app.js] componentDidMount');
  }

  shouldComponentUpdate(nextProps, nextState) {
    console.log('[app.js] shouldComponentUpdate');
    return true;
  }

  componentDidUpdate() {
    console.log('[app.js] componentDidUpdate');
  }

  nameChangeHandler = (event, index) => {
    event.preventDefault();
    const persons = [...this.state.persons]
    persons[index].name = event.target.value
    this.setState((prevState, props) => {
      return {
        persons: persons,
        changeCounter: prevState.changeCounter + 1
      }
    })
  }

  deletePersonHandler = index => {
    const persons = [...this.state.persons]
    persons.splice(index, 1)
    this.setState({persons: persons})
  }

  togglePersons = () => {
    this.setState({
      showPersons: !this.state.showPersons
    })
  }

  loginHandler = () => {
    this.setState({authenticated: true})
  }

  render() {
    console.log('[App.js] render');
    let persons = null;
    if (this.state.showPersons) {
      persons = (
        <div>
          <Persons
            persons={this.state.persons}
            clicked={this.deletePersonHandler}
            changed={this.nameChangeHandler}
          ></Persons>
        </div>
      )
    }

    return (
      <WithClass classes={classes.App}>
        <AuthContext.Provider value={{
            authenticated: this.state.authenticated,
            login: this.loginHandler
        }}>
          <Cockpit
            title={this.props.appTitle}
            showPersons={this.state.showPersons}
            persons={this.state.persons}
            clicked={this.togglePersons}
          ></Cockpit>
          {persons}
        </AuthContext.Provider>
      </WithClass>
    )
  }
}

export default App
